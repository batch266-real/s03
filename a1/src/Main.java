import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed:");

        int num = 1;
        int factorial = 1;

        try{
            num = input.nextInt();

            for(int i = 1; i <= num; i++){
                factorial = factorial * i;
            }
            System.out.println("The factorial of "+ num +" is "+ factorial);

        } catch (Exception e){
            System.out.println("Input must be number!");
        }
    }
}