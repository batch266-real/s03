package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        // Exceptions
        // a problem that arises the execution of the program
        // it disrupts the normal flor of the program and terminate abnormally

        // Exception Handling
        // refers to managing and catching run-time errors to safety run your code.

        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;

        // System.out.println("Enter a number:");

        // try-catch-finally

        try{
            System.out.println("Enter a number1: ");
            num1 = input.nextInt();
            System.out.println("Enter a number2: ");
            num2 = input.nextInt();

            System.out.println("The quotient is: "+ num1/num2);
        } catch (ArithmeticException e){
            System.out.println("You cannot divide a whole number to 0");
        } catch (InputMismatchException e){
            System.out.println("Please input numbers only!");
        } catch (Exception e){
            System.out.println("Something went wrong. Please try again");
        } finally {
            System.out.println("This will execute no matter what");
        }
    }
}
